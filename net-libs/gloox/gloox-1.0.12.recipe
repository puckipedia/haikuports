SUMMARY="Full-featured Jabber/XMPP client library"
DESCRIPTION="
Rock-solid, full-featured Jabber/XMPP client library, written in clean ANSI C++.
"
HOMEPAGE="http://camaya.net/gloox/"
LICENSE="GNU GPL v2"
COPYRIGHT="2002-2012 Jakob Schröter"
SRC_URI="http://camaya.net/download/gloox-$portVersion.tar.bz2"
CHECKSUM_SHA256="67125a2e98a9803179af75f1c9c3c0a14f0bc238d48f23c4a3cf7c8fdebc43a9"
REVISION="1"
ARCHITECTURES="x86 x86_64"
if [ $effectiveTargetArchitecture != x86_gcc2 ]; then
	# x86_gcc2 is fine as primary target architecture as long as we're building
	# for a different secondary architecture.
	ARCHITECTURES="$ARCHITECTURES x86_gcc2"
else
	ARCHITECTURES="$ARCHITECTURES !x86_gcc2"
fi
SECONDARY_ARCHITECTURES="!x86_gcc2 x86"
PATCHES="gloox-1.0.9.patch"

PROVIDES="
	gloox$secondaryArchSuffix = $portVersion
	lib:libgloox$secondaryArchSuffix = 12.0.0 compat >= 12
	"

REQUIRES="
	haiku$secondaryArchSuffix >= $haikuVersion
	lib:libssl$secondaryArchSuffix
	lib:libcrypto$secondaryArchSuffix
	"
BUILD_REQUIRES="
	haiku${secondaryArchSuffix}_devel >= $haikuVersion
	devel:libssl$secondaryArchSuffix
	devel:libcrypto$secondaryArchSuffix
	"
BUILD_PREREQUIRES="
	cmd:gcc$secondaryArchSuffix
	cmd:make
	cmd:libtoolize
	cmd:aclocal
	cmd:autoconf
	cmd:automake
	cmd:pkg_config$secondaryArchSuffix
	"
BUILD()
{
	autoreconf -fi
	runConfigure ./configure
	make
}

INSTALL()
{
	make install
	
	# prepare development lib links
	prepareInstalledDevelLib libgloox
	
	# devel package
	packageEntries devel $developDir
}

TEST()
{
	make check
}


# ----- devel package -------------------------------------------------------

PROVIDES_devel="
	gloox${secondaryArchSuffix}_devel = $portVersion
	devel:libgloox$secondaryArchSuffix = 12.0.0 compat >= 12
	"
REQUIRES_devel="
	gloox$secondaryArchSuffix == $portVersion base
	"
