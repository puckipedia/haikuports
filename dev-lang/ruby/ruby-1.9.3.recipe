SUMMARY="A programming language focused on simplicity and productivity."
DESCRIPTION="Ruby is a dynamic, reflective, object-oriented, general-purpose \
programming language. It was designed and developed in the mid-1990s \
by Yukihiro 'Matz' Matsumoto in Japan.

Ruby embodies syntax inspired by Perl with Smalltalk-like features and \
was also influenced by Eiffel and Lisp.  It supports multiple \
programming paradigms, including functional, object oriented, and \
imperative. It also has a dynamic type system and automatic memory \
management. Therefore, it is similar in varying degrees to, Smalltalk, \
Python, Perl, Lisp, Dylan, and CLU."
HOMEPAGE="http://www.ruby-lang.org" 
SRC_URI="http://cache.ruby-lang.org/pub/ruby/ruby-1.9.3-rc1.tar.bz2"
SOURCE_DIR="ruby-1.9.3-rc1"
CHECKSUM_SHA256="951a8810086abca0e200f81767a518ee2730d6dc9b0cc2c7e3587dcfc3bf5fc8"
LICENSE="Ruby
	GNU GPL v2
	GNU LGPL v2.1
	Artistic (Perl)
	BSD (3-clause)"
COPYRIGHT="1993-2009 Yukihiro Matsumoto"
REVISION="4"
ARCHITECTURES="x86 !x86_64"
if [ $effectiveTargetArchitecture != x86_gcc2 ]; then
    # x86_gcc2 is fine as primary target architecture as long as we're building
    # for a different secondary architecture.
    ARCHITECTURES="$ARCHITECTURES x86_gcc2"
else
    ARCHITECTURES="$ARCHITECTURES !x86_gcc2"
fi
SECONDARY_ARCHITECTURES="x86"

PATCHES="ruby-1.9.3-rc1.patch"

PROVIDES="
	ruby$secondaryArchSuffix = $portVersion compat >= 1.9
	cmd:ruby$secondaryArchSuffix = $portVersion compat >= 1.9
	cmd:erb$secondaryArchSuffix = $portVersion compat >= 1.9
	cmd:gem$secondaryArchSuffix = $portVersion compat >= 1.9
	cmd:irb$secondaryArchSuffix = $portVersion compat >= 1.9
	cmd:rake$secondaryArchSuffix = $portVersion compat >= 1.9
	cmd:rdoc$secondaryArchSuffix = $portVersion compat >= 1.9
	cmd:ri$secondaryArchSuffix = $portVersion compat >= 1.9
	cmd:testrb$secondaryArchSuffix = $portVersion compat >= 1.9
	lib:libruby$secondaryArchSuffix = $portVersion compat >= 1.9
	cmd:ruby = $portVersion compat >= 1.9
	cmd:erb = $portVersion compat >= 1.9
	cmd:gem = $portVersion compat >= 1.9
	cmd:irb = $portVersion compat >= 1.9
	cmd:rake = $portVersion compat >= 1.9
	cmd:rdoc = $portVersion compat >= 1.9
	cmd:ri = $portVersion compat >= 1.9
	cmd:testrb = $portVersion compat >= 1.9
	"

REQUIRES="
	haiku$secondaryArchSuffix >= $haikuVersion
	lib:libedit$secondaryArchSuffix
	lib:libz$secondaryArchSuffix
	lib:libcrypto$secondaryArchSuffix
	lib:libssl$secondaryArchSuffix
	lib:libiconv$secondaryArchSuffix
	lib:libncursesw$secondaryArchSuffix
	"
	
BUILD_REQUIRES="
	devel:libedit$secondaryArchSuffix
	devel:libz$secondaryArchSuffix
	devel:libcrypto$secondaryArchSuffix
	devel:libssl$secondaryArchSuffix
	devel:libiconv$secondaryArchSuffix
	devel:libncursesw$secondaryArchSuffix
	cmd:pkg_config$secondaryArchSuffix
	cmd:ruby >= 1.9
	"

BUILD_PREREQUIRES="
	haiku${secondaryArchSuffix}_devel >= $haikuVersion
	cmd:gcc$secondaryArchSuffix
	cmd:g++$secondaryArchSuffix
	cmd:autoconf
	cmd:make
	cmd:bison
	"

PROVIDES_devel="
	ruby${secondaryArchSuffix}_devel = $portVersion
	devel:libruby_static$secondaryArchSuffix = $portVersion compat >= 1.9
	lib:libruby_static$secondaryArchSuffix = $portVersion compat >= 1.9
	devel:libruby$secondaryArchSuffix = $portVersion compat >= 1.9
	"

REQUIRES_devel="
	ruby$secondaryArchSuffix == $portVersion base
	"

BUILD()
{
	autoconf

	# tell configure where to find the private header folders
	# that have elf32.h, elf64.h, and arch_elf.h
	extraFlags="-I/boot/system/develop/headers/private/system \
			-I/boot/system/develop/headers/private/system/arch/x86"
	export CFLAGS="$extraFlags"
	export CPPFLAGS="$extraFlags"

	runConfigure ./configure --enable-shared
	make $jobArgs
}

INSTALL()
{
	make install
	prepareInstalledDevelLibs libruby-static libruby
	fixPkgconfig
	packageEntries devel $developDir
}

TEST()
{
	make check
}
